import unittest
from selenium import webdriver


class Screenshot(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_screenshot(self):
        driver = self.driver
        filename = 'screenshot.png'
        driver.get('https://simbirsoft.com/')
        driver.maximize_window()
        driver.get_screenshot_as_file(filename)
        print ('FileName:\t{}'.format(__file__))

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
